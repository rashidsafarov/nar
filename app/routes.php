<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// default timezone
date_default_timezone_set('Asia/Baku');
mb_internal_encoding("UTF-8");

Route::pattern('id', '[0-9]+');

App::missing(function(){
    return View::make('site.404');
});

#ADMIN
Route::controller('cp/product/batch', 'adminProductBatchController');
Route::controller('cp/product', 'adminProductController');
Route::controller('cp', 'adminIndexController');

#SITE
Route::get('/product/{any}/{id}', ['uses' =>'ProductController@getSinglepage', 'as'=>'single_product_page']);
Route::controller('/product', 'ProductController');
Route::controller('/cart', 'CartController');


Route::controller('/', 'HomeController');



