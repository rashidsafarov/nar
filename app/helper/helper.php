<?php

function p($var,  $exit = true, $return = false) {

    $result = print_r($var, true);

    if(is_bool($result)) {
        var_dump($result);exit;
    }
    if($return) {
        return $result;
    }
    echo '<pre>'.$result.'</pre>';
    if($exit) die;

}

function mysql_date($time = 0, $hours = true)
{
    $time = $time ? $time : time();
    if ($hours)
        return date('Y-m-d H:i:s', $time);
    else
        return date('Y-m-d', $time);
}

function alertize($text, $type = 'danger')
{
    if (is_string($text)) {
        $alert = $text;
    } else {
        $alert = '';
        $ms = json_decode(json_encode($text), 1);
        foreach ($ms as $msg) {
                $alert .= $msg[0]."<br>";
        }
    }

    return '<span class="alert alert-'.$type.' alert-dismissible fade in">'.$alert.'</span>';
}

function buildTree(array $elements, $parentId = 0) {
    $branch = [];

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
}


function optGroup($array, $selected = false, $level = 0) {

    $result = '';
    foreach($array as $value) {
        if(isset($value['children'])) {
            $result .= '<optgroup label="'.$value['name'].'">';
            $result .= optGroup($value['children'], $selected, $level+1);
            $result .= '</optgroup>';
        }
        else {
            $padding = str_repeat('&nbsp;&nbsp;&nbsp;', $level);
            $result .= '<option value="'.$value['id'].'"'.( $selected && $selected == $value['id'] ? ' selected="selected"' : '' ).'>'.$padding.$value['name'].'</option> ';
        }

    }
    return $result;
}

function buildNav($array, $level = 0) {

    $result = '';
    foreach($array as $value) {
        if(isset($value['children'])) {
            $class = $level == 0 ? 'dropdown' : 'dropdown-submenu';
            $caret = $level == 0 ? ' <span class="caret"></span>' : '';
            $result .= '<li class="'.$class.'">
                    <a href="#"
                    class="dropdown-toggle"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false">'.$value['name'].$caret.'</a>';
            $result .= '<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">';
            $result .= buildNav($value['children'], $level+1);
            $result .= '</ul></li>';
        }
        else {
            $result .= '<li><a href="#categiory_'.$value['id'].'">'.$value['name'].'</a></li>';
        }

    }
    return $result;
}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}


 function resize_image($source_img, $dst_img, $maxWidth, $maxHeight) {

    list($sourceImageWidth, $sourceImageHeight, $sourceImageType) = getimagesize($source_img);

    if ($sourceImageWidth >= $sourceImageHeight && $sourceImageHeight > $maxHeight) {
        $newHeight = $maxHeight;
        $newWidth  = $maxHeight * ($sourceImageWidth / $sourceImageHeight);
    } elseif ($sourceImageWidth <= $sourceImageHeight && $sourceImageWidth > $maxWidth) {
        $newWidth  = $maxWidth;
        $newHeight = $maxWidth * ($sourceImageHeight / $sourceImageWidth);
    } else {
        $newWidth  = $sourceImageWidth;
        $newHeight = $sourceImageHeight;
    }

    $image = imagecreatetruecolor($newWidth, $newHeight);


    switch ($sourceImageType) {
        case IMAGETYPE_GIF:
            $source = imagecreatefromgif($source_img);
            imagecopyresized($image, $source, 0, 0, 0, 0, $newWidth, $newHeight, $sourceImageWidth, $sourceImageHeight);
            imagejpeg($image, $dst_img);
            break;
        case IMAGETYPE_JPEG:
            $source = imagecreatefromjpeg($source_img);
            imagecopyresized($image, $source, 0, 0, 0, 0, $newWidth, $newHeight, $sourceImageWidth, $sourceImageHeight);
            imagejpeg($image, $dst_img);
            break;
        case IMAGETYPE_PNG:
            $source = imagecreatefrompng($source_img);
            $white  = imagecolorallocate($image, 255, 255, 255);
            imagefilledrectangle($image, 0, 0, $newWidth, $newHeight, $white);
            imagecopyresized($image, $source, 0, 0, 0, 0, $newWidth, $newHeight, $sourceImageWidth, $sourceImageHeight);
            imagejpeg($image, $dst_img);
            break;
    }
}

function user_ip() {
    if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
        return $_SERVER['HTTP_CF_CONNECTING_IP'];
    } elseif ($_SERVER['HTTP_X_FORWARDED_FOR']) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        return $_SERVER['REMOTE_ADDR'];
    }
}

function _t($key) {
   echo Lang::get($key);
}