<?php

class BaseModel extends Eloquent
{
    protected $rules = array();

    protected $errors;

    public function validate($data = []) {

        if($data == []){
            $data = $this->getAttributes();
        }

        $v = Validator::make($data, $this->rules);

        if($v->fails()) {

            $this->errors = $v->errors();

            return false;
        }

        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public function setRule($name, $rule) {
        $this->rules[$name] = $rule;
    }

}