<?php

class Category extends Eloquent
{

    protected $table = 'category';
    protected $primaryKey = 'id';

    public function createBreadcrumbs() {

        if(!$this->parent_id) {
            return $this->getLink();
        } else {
            return $this->find($this->parent_id)->createBreadcrumbs().'/'.$this->getLink();
        }

    }

    public function getLink() {
        return '<a href="#category_'.$this->id.'"">'.$this->name.'</a>';
    }

}