<?php

class product_batch extends BaseModel
{

    protected $table = 'product_batch';
    protected $primaryKey = 'id';

    protected $rules = [
        'product_id'  => 'required|exists:product,id',
        'batch_no'    => 'required',
        'size'       => 'required|min:1|max:50',
        'color'       => 'required|min:3|max:50',
        'qty'       => 'required|integer|min:1'
    ];


    public function product() {
        return $this->belongsTo('Product', 'product_id');
    }
}