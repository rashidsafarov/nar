<?php

class Product extends BaseModel
{

    protected $table = 'product';
    protected $primaryKey = 'id';

    protected $rules = [
        'category_id' => 'required|exists:category,id',
        'code'        => 'required|unique:product,code',
        'title'       => 'required|min:6|max:50',
        'price'       => 'required|numeric',
        'description' => 'required|min:6|max:1000'
    ];

    public function category() {
        return $this->hasOne('Category', 'id', 'category_id');
    }

    public function currency() {
        return $this->hasOne('currency', 'id', 'currency_id');
    }

    public function batches() {
        return $this->hasMany('product_batch', 'product_id');
    }

    public function photos() {
        return $this->hasMany('product_photo', 'product_id');
    }

    public function reviews() {
        return $this->hasMany('product_review', 'product_id');
    }

    public function tags() {
        return $this->hasMany('product_tag', 'product_id');
    }

    public function quantity() {
        return $this->batches->sum('qty');
    }

    public function price() {
        return $this->currency->format($this->price);
    }

    public function colors() {
        return array_unique($this->batches->lists('color'));
    }

    public function sizes() {
        return array_unique($this->batches->lists('size'));
    }

    public function ratingAvg() {
        return $this->reviews()->avg('rating');
    }

    public function ratingCount() {
        return $this->reviews->count();
    }

    public function url() {
        return url().'/product/'.slugify($this->title).'/'.$this->id;
    }

    public function tags_list($with_link = false) {

        $tags = $this->tags;
        $result = [];
        foreach($tags as $tagObj) {
            $result[] = !$with_link ? $tagObj->tag : $tagObj->tagWithLink();
        }

        return implode(', ', $result);
    }

    public function saveTags($tags_str) {

        //Delete old tags
        product_tag::where('product_id', '=', $this->id)->delete();

        //sanitize
        $tags = explode(',', $tags_str);
        foreach($tags as &$tag) {
            $tag = ucfirst(strtolower(trim($tag)));
        }

        // Make bulk insert
        $data_to_insert = [];
        foreach(array_unique($tags) as $t) {
            $data_to_insert[] = [
                'product_id' => $this->id,
                'tag'   => $t
            ];
        }
        product_tag::insert($data_to_insert);
    }

    public function options() {
        $data = $this->get(['id', 'title', 'code'])->toArray();
        $options = [];
        foreach($data as $row) {
            $options[$row['id']] = $row['code'].' - '.$row['title'];
        }

        return $options;
    }


}