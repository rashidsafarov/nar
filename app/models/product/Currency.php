<?php

/**
 *  Default currency is USD
 *  Class Currency
 */
class Currency extends BaseModel
{
    protected $table = 'currency';
    protected $primaryKey = 'id';

    private $default_currency = 1;

    public function format($amount) {

        // User currency setting
        $id = (int) Session::get('currency');
        if(!$id || !($currencyObj = Currency::find($id))) {
            $currencyObj = Currency::find($this->default_currency);
        }
        $amount = number_format($amount * $currencyObj->rate / $this->rate, 2);

        return $currencyObj->sign.$amount;
    }

    public function options() {
        $data = $this->get(['id','code','sign'])->toArray();
        $options = [];
        foreach($data as $row) {
            $options[$row['id']] = $row['sign'].$row['code'];
        }
        return $options;
    }

}