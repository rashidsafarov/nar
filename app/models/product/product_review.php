<?php

class product_review extends BaseModel
{

    protected $table = 'product_review';
    protected $primaryKey = 'id';

    protected $rules = [
        'product_id'  => 'required',
        'rating'      => 'required|integer|min:1|max:5'
    ];



}