<?php

class product_photo extends BaseModel
{

    protected $table = 'product_photo';
    protected $primaryKey = 'id';

    private $supportedFormats = ['png', 'jpeg', 'jpg', 'gif'];
    private $imageMaxHeight = 683;
    private $imageMaxWidth = 657;
    private $imageThumbWidth = 144;
    private $imageThumbHeight = 158;
    private $fileFolder = 'photos';

    public $inputFile;


    public function large() {
        return asset($this->fileFolder.'/'.$this->name);
    }

    public function thumb() {
        return asset($this->fileFolder.'/small_'.$this->name);
    }

    private function filePath($file = '') {
        return public_path().'/'.$this->fileFolder.'/'.$file;
    }

    public function setFile($Input) {


    }

    /**
     * todo check also image byte size
     * @return bool
     */
    public function validate($data = []) {

        $fileExtention = explode('/', $this->inputFile->getMimeType())[1];
        return in_array($fileExtention, $this->supportedFormats);
    }

    public function save(array $options = []) {

        // File name
        $this->name = str_random(9) . '.' . 'jpg';
        // Move to path
        $this->inputFile->move($this->filePath(), $this->name);

        // Resize image
        resize_image($this->filePath($this->name), $this->filePath($this->name), $this->imageMaxWidth, $this->imageMaxHeight);
        //thumb
        resize_image($this->filePath($this->name), $this->filePath('small_'.$this->name), $this->imageThumbWidth, $this->imageThumbHeight);

        parent::save($options);
    }






}