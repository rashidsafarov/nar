<?php

class BaseController extends Controller
{

    public function __construct() {

        // Global data
        View::share([
            'currencies' =>  Currency::all(),
            'user_currency' => (int) Session::get('currency') ? Session::get('currency') : (new Currency())->default_currency,
            'menu_nav' => buildNav(buildTree(Category::all()->toArray())),
            'cart_item_count' => count(Session::get('cart_items'))
        ]);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if(!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }


}
