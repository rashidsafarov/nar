<?php

use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\FiltersRow;

/**
 * Class adminProductBatchController
 */
class adminProductBatchController extends \BaseAdminController
{
    public function getCreate() {

        return View::make('admin.product.batch_create',
            [
                'products' => (new Product())->options(),
                'product_id' => Input::get('product_id')
            ]);
    }

    public function postCreate() {

        $data = Input::all();
        $batch = new product_batch();
        $batch->product_id = $data['product_id'];
        $batch->batch_no = $data['batch_no'];
        $batch->size = $data['size'];
        $batch->color = $data['color'];
        $batch->qty = $data['qty'];

        if($batch->validate()) {

            $batch->save();

            $msg = alertize('Batch has been successfully registered', 'success');
            return Redirect::to('cp/product/batch')->withErrors($msg);
        }

        $msg = alertize($batch->errors()->first());
        return Redirect::to('cp/product/batch/create')->withInput()->withErrors($msg);

    }

    public function getUpdate($id) {

        $batch = product_batch::find($id);
        if(!$batch) return Redirect::to('cp/product/batch');

        return View::make('admin.product.batch_create',
            [
                'batch' => $batch,
                'products' => (new Product())->options()
            ]);
    }

    public function postUpdate($id) {

        $batch = product_batch::find($id);
        if(!$batch) return Redirect::to('cp/product/batch');

        $data = Input::all();
        $batch->product_id = $data['product_id'];
        $batch->batch_no = $data['batch_no'];
        $batch->size = $data['size'];
        $batch->color = $data['color'];
        $batch->qty = $data['qty'];

        if($batch->validate()) {

            $batch->save();

            $msg = alertize('Batch has been successfully updated', 'success');
            return Redirect::to('cp/product/batch/update/'.$id)->withErrors($msg);
        }

        $msg = alertize($batch->errors()->first());
        return Redirect::to('cp/product/batch/update/'.$id)->withInput()->withErrors($msg);

    }

    public function getDelete($id) {

//        $batch = product_batch::find($id);
//        if($batch) {
//            $batch->active = 0;
//            $batch->save();
//        }

        return Redirect::to('cp/product/batch');
    }

    public function getIndex() {

        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider(product_batch::query())
                )
                ->setName('batches_grid')
                ->setColumns([
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('ID')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('batch_no')
                        ->setLabel('Batch_no')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('product.title')
                        ->setLabel('Product')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('size')
                        ->setLabel('Size')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('color')
                        ->setLabel('Color')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('qty')
                        ->setLabel('Quantity')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $delete_icon = '<a href="/cp/product/batch/delete/'.$val.'" class="p-delete fa fa-2x fa-trash" title="Delete"></a>&nbsp;&nbsp;&nbsp;';
                            $edit_icon = '<a href="#"  data-id="'.$val.'" class="p-edit fa fa-2x fa-pencil" title="Edit"></a>&nbsp;&nbsp;&nbsp;';

                            return $edit_icon.$delete_icon;

                        })
                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            (new RecordsPerPage),
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                        ])
                ])
        );
        $grid = $grid->render();

        return View::make('admin.product.batches',
            [
                'grid' => $grid
            ]);
    }
}