<?php

use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\FiltersRow;

/**
 * Class adminProductController
 */
class adminProductController extends \BaseAdminController
{

    public function getCreate() {

        return View::make('admin.product.create',
            [
                'currencies'   => (new Currency())->options(),
                'categoryOpts' => optGroup(buildTree(Category::all()->toArray()))
            ]);
    }

    public function postCreate() {

        $data = Input::all();
        $product = new Product();
        $product->category_id = $data['category_id'];
        $product->code = $data['code'];
        $product->title = $data['title'];
        $product->price = $data['price'];
        $product->currency_id = $data['currency_id'];
        $product->description = $data['description'];

        if($product->validate()) {

            $product->save();

            //saving tags
            $product->saveTags($data['tags']);

            //saving images
            $files = Input::file('file');
            if ($files) {
                foreach($files as $file) {
                    $photo = new product_photo();
                    $photo->product_id = $product->id;
                    $photo->inputFile = $file;
                    $photo->save();
                }
            }

            $msg = alertize('Item has been successfully registered', 'success');

            return Redirect::to('cp/product')->withErrors($msg);
        }

        // Sending error
        $msg = alertize($product->errors()->first());

        return Redirect::to('cp/product/create')->withInput()->withErrors($msg);

    }

    public function getUpdate($id) {

        $product = Product::find($id);
        if(!$product) return Redirect::to('cp/products');

        return View::make('admin.product.create',
            [
                'product'      => $product,
                'currencies'   => (new Currency())->options(),
                'categoryOpts' => optGroup(buildTree(Category::all()->toArray()), $product->category_id),
            ]);
    }

    public function postUpdate($id) {

        $product = Product::find($id);
        if(!$product) return Redirect::to('cp/products');

        $data = Input::all();
        $product->category_id = $data['category_id'];
        $product->code = $data['code'];
        $product->title = $data['title'];
        $product->price = $data['price'];
        $product->currency_id = $data['currency_id'];
        $product->description = $data['description'];

        $product->setRule('code', 'required|unique:product,code,'.$product->id);
        if($product->validate()) {

            $product->save();

            //setting tags
            $product->saveTags($data['tags']);

            //saving images
            $files = Input::file('file');
            if ($files) {
                foreach($files as $file) {
                    if($file == '') continue;
                        $photo = new product_photo();
                        $photo->product_id = $product->id;
                        $photo->inputFile = $file;
                        $photo->save();
                }
            }

            $msg = alertize('Item has been successfully updated', 'success');

            return Redirect::to('cp/product/update/'.$id)->withErrors($msg);
        }

        // Sending error
        $msg = alertize($product->errors()->first());

        return Redirect::to('cp/product/update/'.$id)->withInput()->withErrors($msg);
    }

    public function getDelete($id) {
        $product = Product::find($id);
        if($product) {
            $product->active = 0;
            $product->save();
        }

        return Redirect::to('cp/product');
    }

    public function getIndex() {
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider(Product::where('active', 1))
                )
                ->setName('products_grid')
                ->setColumns([
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('ID')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('code')
                        ->setLabel('Product code')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('category.name')
                        ->setLabel('Category')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_LIKE)),
                    (new FieldConfig)
                        ->setName('title')
                        ->setLabel('Title')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_LIKE)),
                    (new FieldConfig)
                        ->setName('price')
                        ->setLabel('Price')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                        ->addFilter((new FilterConfig)->setOperator(FilterConfig::OPERATOR_EQ)),
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val, $row) {
                            $delete_icon = '<a href="/cp/product/delete/'.$val.'" class="p-delete fa fa-2x fa-trash" title="Delete"></a>&nbsp;&nbsp;&nbsp;';
                            $edit_icon = '<a href="#"  data-id="'.$val.'" class="p-edit fa fa-2x fa-pencil" title="Edit"></a>&nbsp;&nbsp;&nbsp;';
                            $view_icon = '<a href="#" data-url="'.$row->getSrc()->url().'" class="p-view fa fa-2x fa-eye" title="Website view" style="vertical-align:bottom"></a>&nbsp;&nbsp;&nbsp;';
                            $add_icon = '<a href="/cp/product/batch/create?product_id='.$val.'" data-id="'.$val.'" class="fa fa-2x fa-plus" title="Add batch" style="vertical-align:bottom"></a>&nbsp;&nbsp;&nbsp;';

                            return $edit_icon.$add_icon.$delete_icon.$view_icon;

                        })
                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            (new RecordsPerPage),
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                        ])
                ])
        );
        $grid = $grid->render();

        return View::make('admin.product.products',
            [
                'grid' => $grid
            ]);
    }

    private function addImage($product_id) {


    }
}