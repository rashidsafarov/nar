<?php

class adminIndexController extends \BaseAdminController
{
    function __construct()
    {
       // $this->beforeFilter('auth');
    }

    public function getIndex()
    {
        return View::make('adminLayout');
    }
}