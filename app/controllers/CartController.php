<?php

class CartController extends BaseController
{

    public function getIndex() {
        return Redirect::to('/');
    }

    public function getAdd() {
        return $this->postAdd();
    }

    public function postView() {
        if(Request::ajax()) {
            $cart_items = (array) Session::get('cart_items');
            $data = [];
            $i = 0;
            foreach($cart_items as $item) {
                if($i > 2) break;
                $product = Product::find($item['id']);
                if($product) {
                    $data[$i]['id'] = $product->id;
                    $data[$i]['title'] = $product->title;
                    $data[$i]['price'] = $product->price();
                    $data[$i]['color'] = ucfirst($item['color']);
                    $data[$i]['size'] = strtoupper($item['size']);
                    $data[$i]['qty'] = $item['qty'];
                    $data[$i]['photo'] = $product->photos[0]->thumb();
                    $i++;
                }
            }
            return View::make('site.cart_dropdown', [
                'cart_items' => $data
            ]);
        }
        return null;
    }

    public function getEmpty() {
        if(Request::ajax()) {
            Session::forget('cart_items');
        }
    }

    public function postAdd() {
        if(Request::ajax()) {

            // current items in cart
            $cart_items = (array) Session::get('cart_items');

            // Input data
            $id = Input::get('id');
            $size = (string)Input::get('size');
            $color = (string)Input::get('color');
            $qty = (string)Input::get('qty');

            // item key
            $key = md5($id.$size.$color);

            $stock_qty = product_batch::where(['product_id'=>$id, 'size'=>$size, 'color'=>$color])->sum('qty');
            if($stock_qty >= $qty) {

                // cart array
                if( isset($cart_items[$key]) && $stock_qty >= $qty + $cart_items[$key]['qty']) {

                    $cart_items[$key]['qty'] += $qty;
                    Session::set('cart_items', $cart_items);
                    return Response::json(['status' => 'success', 'msg' => ($qty).' items added']);

                } elseif( isset($cart_items[$key]) && $stock_qty < $qty + $cart_items[$key]['qty']) {

                    return Response::json(['status' => 'error', 'msg' => 'There is no items left in stock']);

                } elseif( !isset($cart_items[$key]) && $stock_qty >= $qty) {

                    $cart_items[$key]['id'] = $id;
                    $cart_items[$key]['qty'] = $qty;
                    $cart_items[$key]['size'] = $size;
                    $cart_items[$key]['color'] = $color;
                    Session::set('cart_items', $cart_items);
                    return Response::json(['status' => 'success', 'msg' => ($qty).' items added']);

                } elseif( !isset($cart_items[$key]) && $stock_qty < $qty) {
                    return Response::json(['status' => 'error', 'msg' => 'There is no items left in stock']);
                }

            } else {
                return Response::json(['status' => 'error', 'msg' => 'There is no items left in stock']);
            }

        }
    }
}
