<?php

class HomeController extends BaseController
{

    public function getIndex() {
       return Redirect::to('product/title_goes_here/3');
    }

    public function postSetcurrency() {
        if(Request::ajax()) {
            $currency_id = Input::get('value');
            if(Currency::find($currency_id)) {
                Session::set('currency', $currency_id);
            }
        }
    }
}
