<?php

class ProductController extends BaseController
{

    public function getIndex() {
        return View::make('site.product.all');
    }

    public function getSinglepage($slug, $id) {

       if(!($product = Product::find($id))) return Redirect::to('404');


        return View::make('site.product.single_page', [
            'product' => $product,
            'is_wishlisted' => isset(Session::get('wishlist')[$id])
        ]);
    }

    public function postVote() {
        if(Request::ajax()) {
            $id = Input::get('id');
            $rate = Input::get('rate');
            $product = Product::find($id);
            if($product) {

                $rating = new product_review();
                $rating->product_id = $id;
                $rating->rating = $rate;
                $rating->ip = Request::ip();
                if($rating->validate())
                    $rating->save();

                return Response::json([
                    'count' => $product->ratingCount(),
                    'avg' => $product->ratingAvg()
                ]);
            }
        }

    }

    public function getWishlistadd() {
        if(Request::ajax()) {
            $product_id = (int)Input::get('id');
            $wishlist = (array)Session::get('wishlist');
            $wishlist[$product_id] = 1;
            Session::put('wishlist', $wishlist);

        }
    }

    public function getWishlistremove() {
        if(Request::ajax()) {
            $product_id = (int)Input::get('id');
            $wishlist = (array)Session::get('wishlist');
            if(isset($wishlist[$product_id]))
                unset($wishlist[$product_id]);
            Session::put('wishlist', $wishlist);
        }
    }


}
