$(function () {

    // Header currency change listener
    $('#currency').on('change', function () {
        var selected = $(this).find("option:selected").val();
        $.post('/setcurrency', {'value': selected}, function () {
            window.location.reload();
        });
    });

    // Photo thuumb large
    $('.product-photos a').on('click', function () {
        $('.product-main-photo img').attr('src', $(this).data('large'));
    });

    // Star rating plugin listener
    $('.product-rating').starrr({
        readOnly: true,
        change: function (e, value) {
            var container = $(this).closest('.product-rating-container');
            $.post('/product/vote', {'id': $(this).data('id'), 'rate': value}, function (data) {
                $('.sr-solely b', container).html(data.count);
                $('input[type="hidden"]', container).val(data.avg);
                //   window.location.reload();
            });
        }
    });

    //wishlist search
    $('#search-wrapper button').on('click', function(e) {
        e.preventDefault();
        $('#search-wrapper input[type="text"]').show().animate({width: '200px'}).focus();
    });
    $('#search-wrapper input[type="text"]').on('blur', function(e) {
        if($(e.relatedTarget).hasClass('fa-search')) return;
        $('#search-wrapper input[type="text"]').animate({width: '0px'}, function(){
            $(this).hide();
        });
    });

    // whishlist add and remove
    $('.btn-add-wishlist, .btn-remove-wishlist').on('click', function() {
        $(this).hide();
        var action = 'add';
        if($(this).hasClass('btn-add-wishlist'))
            $('.btn-remove-wishlist').show();
        else {
            $('.btn-add-wishlist').show();
            action = 'remove';
        }

        $.get('/product/wishlist'+action, {'id' : $(this).data('id')}, function (data) {
            console.log(data);
        });
    });

    // cart button listener
    $('.btn-cart').on('click', function(e){
        if($('.dropdown-cart').is(":visible")){
            $('.dropdown-cart').hide();
        } else {
            $('.dropdown-cart').html('<img src="/images/loader.gif">');
            $('.dropdown-cart').show();
            $.post('/cart/view', function (data) {
                $('.dropdown-cart').html(data);
            });
        }
    });
    $('html').click(function(e) {
        if(!($(e.target).parents('.cart-container').length > 0)){
            $('.dropdown-cart').hide();
        }
         //
    });

    // chart add
    $('.btn-add-cart').on('click', function () {
        var data = {
            id: $(this).data('id'),
            color: $('#product_color').val(),
            size: $('#product_size').val(),
            qty: $('#product_qty').val()};

        $.post('/cart/add', data, function (data) {console.log(data);
            if(data.status == 'success'){

                var count = parseInt($('.cart-container .count').html());
                $('.cart-container .count').text(count+1);
                $('.cart-container').blink();
            }


        });
    });

});